#[derive(Serialize, Deserialize, Debug)]
pub struct Object {
    name: String,
    value: String,
    stationcount: String,
}
